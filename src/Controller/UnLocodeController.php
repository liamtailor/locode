<?php

namespace App\Controller;

use App\Entity\UnLocode;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UnLocodeController extends AbstractController
{

    /**
     * @Route("/locode/get_by_code/{country_code}{location_code}", name="un_locode_get_by_code_edge")
     */
    public function getEntityByLocodeEdge(Request $request): JsonResponse
    {
        $repository = $this->getDoctrine()->getRepository(UnLocode::class);
        // TODO: Validate request data
        $countryCode = $request->query->get('country_code');
        $locationCode = $request->query->get('location_code');
        if (is_null($countryCode) || is_null($locationCode)) throw $this->getInvalidEdgeParametersException();

        $unLocode = $repository->findOneByLocode($countryCode, $locationCode);
        if (is_null($unLocode)) throw $this->createNotFoundException('Invalid country or location code.');;

        return $this->json($unLocode);
    }

    /**
     * @Route("/locode/get_by_name/{name_without_diacritics}", name="un_locode_get_by_name_without_diacritics")
     */
    public function getEntityByNameWithoutDiacritics(Request $request): JsonResponse
    {
        $repository = $this->getDoctrine()->getRepository(UnLocode::class);
        // TODO: Validate request data
        $nameWithoutDiacritics = str_replace('"', '', $request->query->get('name_without_diacritics'));
        if (is_null($nameWithoutDiacritics)) throw $this->getInvalidEdgeParametersException();

        $unLocode = $repository->findByNameWithoutDiacritics($nameWithoutDiacritics);
        if (is_null($unLocode)) throw $this->createNotFoundException('Invalid name given or not found. Names can\'t contain diacritics!');

        return $this->json($unLocode);
    }

    private function getInvalidEdgeParametersException(): NotFoundHttpException
    {
        return $this->createNotFoundException('Invalid edge parameters given. Try /locode/get_by_code?country_code=FR&location_code=LH2 or /locode/get_by_name?name_without_diacritics=Le Heaulme');
    }
}

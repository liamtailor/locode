<?php

namespace App\Formatter;

class LocodeLocationNameFormatter
{ 
    public static function removeAlternativeName(string &$locationName)
    {
        $locationName = \preg_replace('/\(.*\)?/', '', $locationName);
        $locationName = \preg_replace('/\/.*/', '', $locationName);
        $locationName = \rtrim($locationName);
    }
}

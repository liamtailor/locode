<?php

namespace App\Repository;

use App\Entity\UnLocode;
use App\Formatter\LocodeLocationNameFormatter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method UnLocode|null find($id, $lockMode = null, $lockVersion = null)
 * @method UnLocode|null findOneBy(array $criteria, array $orderBy = null)
 * @method UnLocode[]    findAll()
 * @method UnLocode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UnLocodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UnLocode::class);
    }

    public function findOneByLocode(string $locodeCountry, string $locodeLocation): ?UnLocode
    {
        return $this->createQueryBuilder('l')
            ->where('l.locodeCountry = :locodeCountry')
            ->andWhere('l.locodeLocation = :locodeLocation')
            ->setParameter('locodeCountry', $locodeCountry)
            ->setParameter('locodeLocation', $locodeLocation)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findByNameWithoutDiacritics(string $nameWithoutDiacritics): ?UnLocode
    {
        return $this->createQueryBuilder('l')
            ->where('l.nameWithoutDiacritics = :nameWithoutDiacritics')
            ->setParameter('nameWithoutDiacritics', $nameWithoutDiacritics)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneByCountryCodeAndNameWithoutDiacritics(string $locodeCountry, string $nameWithoutDiacritics): ?UnLocode
    {
        return $this->createQueryBuilder('l')
            ->where('l.locodeCountry = :locodeCountry')
            ->andWhere('l.nameWithoutDiacritics = :nameWithoutDiacritics')
            ->setParameter('locodeCountry', $locodeCountry)
            ->setParameter('nameWithoutDiacritics', $nameWithoutDiacritics)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findByCountryCodeAndNameWithoutDiacriticsOrCode(string $locodeCountry, string $recordNameWithoutDiacriticsOrCode): ?UnLocode
    {
        $names = explode(' = ', $recordNameWithoutDiacriticsOrCode);
        if (count($names) === 2) {
            $refersTo = $names[1];
        } else {
            $refersTo = $recordNameWithoutDiacriticsOrCode;
        }

        LocodeLocationNameFormatter::removeAlternativeName($refersTo);

        if (\preg_match('/[A-Z]{2}\ ?[A-Z]{3}/', $refersTo) === 1) {
            $locode = str_replace(' ', '', $refersTo);
            $locode = [\substr($refersTo, 0, 2), \substr($locode, 2, 3)];
            return $this->findOneByLocode($locode[0], $locode[1]);
        } else {
            return $this->findOneByCountryCodeAndNameWithoutDiacritics($locodeCountry, $refersTo);
        }
    }
}

<?php

namespace App\Repository;

use App\Entity\ReferenceEntry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ReferenceEntry|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReferenceEntry|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReferenceEntry[]    findAll()
 * @method ReferenceEntry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReferenceEntryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReferenceEntry::class);
    }
}

<?php

namespace App\Command;

use App\Entity\UnLocode;
use Symfony\Component\Console\Command\Command;
use App\EntityFactory\ReferenceEntryFactory;
use App\EntityFactory\UnLocodeFactory;
use App\LocodeDatabaseInformation\LocodeDatabaseCurrentCsvZipUrl;
use App\LocodeDatabaseInformation\LocodeDatabaseCurrentIssueDate;
use App\LocodeDatabaseInformation\VariableToFileWriter;
use App\LocodeDatabaseUpdater\LocodeCsvFileParser;
use App\LocodesSourceFileHandler\ZipFileHandler;
use App\Scraper\LocodeWebsiteScraper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportUnLocodeEntitiesToDatabaseCommand extends Command
{
    /** @var EntityManager $em */
    private $em;
    /** @var LocodeWebsiteScraper $locodeWebsiteScraper */
    protected $locodeWebsiteScraper;

    public function __construct(EntityManagerInterface $em, LocodeWebsiteScraper $locodeWebsiteScraper)
    {
        $this->em = $em;
        parent::__construct();
        $this->locodeWebsiteScraper = $locodeWebsiteScraper;
    }

    protected function configure()
    {
        $this
            ->setName('app:import-locode-entities-to-database')
            ->setDescription('Import new UNLOCODE entities from CSV file to database.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $this->setPhpMemoryLimit();
        } catch (\Exception $e) {
            $output->writeln('Error: ' . $e->getMessage());
            return 0;
        }

        $wasLocodeDatabaseUpdated = $this->handleUneceOrgWebsiteUpdate($output);

        if ($wasLocodeDatabaseUpdated) {
            $output->writeln('Reading new CSV files .....');
            // TODO: File names from ZIP should be read with a regex and their contents should be validated (eg. no. of columns). 
            $parser = new LocodeCsvFileParser();
            $resultSets = $this->readCsvFiles($parser);

            $unLocodeRepository = $this->em->getRepository(UnLocode::class);

            $output->writeln('Generating new UnLocode entities .....');
            $unLocodeFactory = new UnLocodeFactory();
            foreach ($resultSets as $resultSet) {
                $unLocodeSets[] = $unLocodeFactory->makeEntitiesFromRecords($resultSet->getRecords());
            }
            $output->writeln('Saving generated UnLocode entities to database .....');
            $this->saveGeneratedEntitiesToDatabase($unLocodeSets);

            /* 
            Important to first save UnLocode entities before generating ReferenceEntry entities.
             ReferenceEntry entities look for UnLocode entities in repository, so they need to be created after UnLocode entries are saved to database.
             */

            $output->writeln('Generating new ReferenceEntry entities .....');
            $referenceEntryFactory = new ReferenceEntryFactory($unLocodeRepository);
            foreach ($resultSets as $resultSet) {
                $referenceEntrySets[] = $referenceEntryFactory->makeEntitiesFromRecords($resultSet->getRecords());
            }
            $output->writeln('Saving generated ReferenceEntry entities to database .....');
            $this->saveGeneratedEntitiesToDatabase($referenceEntrySets);
        } else {
            $output->writeln('No new issued date, so no action required.');
        }
        $output->writeln('Done. Exiting ...');
        return 1;
    }

    private function handleUneceOrgWebsiteUpdate(OutputInterface $output): bool
    {
        $zipFileHandler = new ZipFileHandler(new \ZipArchive, __DIR__ . '/../../locode_csv');
        $variableToFileWriter = new VariableToFileWriter();
        $issuedDate = new LocodeDatabaseCurrentIssueDate($variableToFileWriter);
        $zipFileUrl = new LocodeDatabaseCurrentCsvZipUrl($variableToFileWriter);

        $newIssuedDate = $this->locodeWebsiteScraper->getIssuedDate();
        $newZipFileUrl = $this->locodeWebsiteScraper->getCsvZipFileName();

        if ($issuedDate->getCurrentValue() !== $newIssuedDate) {
            $issuedDate->setNewValue($newIssuedDate);
            $locodeDatabaseWasUpdated = true;
            $output->writeln('Locode database has new issue!');
        } else {
            $locodeDatabaseWasUpdated = false;
        }

        if ($zipFileUrl->getCurrentValue() !== $newZipFileUrl) {
            $zipFileUrl->setNewValue($newZipFileUrl);
            $output->writeln('Locode database file name was changed!');
        }

        if ($locodeDatabaseWasUpdated) $zipFileHandler->unzip($zipFileUrl->getCurrentValue());

        return $locodeDatabaseWasUpdated;
    }

    /** @throws \Exception */
    private function setPhpMemoryLimit(): void
    {
        if ($this->thereIsNotEnoughMemoryAvailable()) {
            $wasMemorySet =  ini_set('memory_limit', '512MB');
            if ($wasMemorySet === false) {
                throw new \Exception('Could not set php memory limit to 512. Set memory_limit = 512M (or more) in php.ini file manually.');
            }
        }
    }

    private function thereIsNotEnoughMemoryAvailable(): bool
    {
        $memory_limit = ini_get('memory_limit');
        if (preg_match('/^(\d+)(.)$/', $memory_limit, $matches)) {
            if ($matches[2] == 'M') {
                $memory_limit = $matches[1] * 1024 * 1024; // nnnM -> nnn MB
            } else if ($matches[2] == 'K') {
                $memory_limit = $matches[1] * 1024; // nnnK -> nnn KB
            }
        }

        return $memory_limit < 512 * 1024 * 1024; // Less than 512MB
    }

    private function readCsvFiles(LocodeCsvFileParser $parser): array
    {
        for ($part = 1; $part <= 3; $part++) {
            $resultSets[] = $parser->getRecordsFromFile(__DIR__ . '/../../locode_csv/2019-1 UNLOCODE CodeListPart' . $part . '.csv');
        }
        return $resultSets ?? [];
    }

    private function saveGeneratedEntitiesToDatabase(array $entitySets): void
    {
        // TODO: Need custom INSERT ON DUPLICATE KEY UPDATE query instead of regular persist/flush since it'll generate duplicate entities and throw errors.
        $batchCount = 0;
        foreach ($entitySets ?? [] as $set) {
            foreach ($set as $entity) {
                $batchCount++;
                $this->em->merge($entity);
                if ($batchCount % 10000 === 0) {
                    $this->flushEntityManager();
                }
            }
        }
        // flush and clear last batch
        $this->flushEntityManager();
    }

    private function flushEntityManager(): void
    {
        $this->em->flush();
        $this->em->clear();
    }
}

<?php

namespace App\Scraper\DomScrapers;

use simple_html_dom;

class CsvZipFileNameScraper extends DomScraper
{
    /** @throws \Exception */
    public function scrape(simple_html_dom $websiteDom): string
    {
        $tBodyNode = $this->getCodesForTradeTableBodyNode($websiteDom);
        $fileName = $tBodyNode->children(2)->children(3)->children(0)->children(2)->getAttribute('href');

        return $fileName;
    }
}

<?php

namespace App\Scraper\DomScrapers;

require_once(__DIR__ . '/../simple_html_dom.php');

use simple_html_dom;
use simple_html_dom_node;

abstract class DomScraper
{
    public abstract function scrape(simple_html_dom $websiteDom): string;

    /** @throws \Exception */
    protected function getCodesForTradeTableBodyNode(simple_html_dom $websiteDom): simple_html_dom_node
    {
        $tableParentDivNode = $websiteDom->find('div[id=c21211]');
        if (empty($tableParentDivNode)) throw new \Exception('The "Codes for Trade" node was not found. Website source migth\'ve been altered.' .
            ' See if <div> element with id="c21211" exists in website source view-source:http://www.unece.org/cefact/codesfortrade/codes_index.html');
        $tableNode = $tableParentDivNode[0]->children(1);
        $tBodyNode = $tableNode->children(0);
        
        return $tBodyNode;
    }
}

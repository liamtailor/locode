<?php

namespace App\Scraper\DomScrapers;

use simple_html_dom;

class IssuedDateScraper extends DomScraper
{
    /** @throws \Exception */
    public function scrape(simple_html_dom $websiteDom): string
    {
        $tBodyNode = $this->getCodesForTradeTableBodyNode($websiteDom);
        $date = $tBodyNode->children(2)->children(1)->innertext;

        return $date;
    }
}

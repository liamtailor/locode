<?php

namespace App\Scraper;

use App\Scraper\DomScrapers\CsvZipFileNameScraper;
use App\Scraper\DomScrapers\IssuedDateScraper;

class LocodeWebsiteScraper
{
    public function __construct(
        string $baseUrl,
        string $urlToScrape,
        SimpleHtmlDomFactory $simpleHtmlDomFactory,
        WebsiteSourceDownloader $websiteSourceDownloader,
        IssuedDateScraper $issuedDateScraper,
        CsvZipFileNameScraper $csvZipFileNameScraper
    ) {
        $source = $websiteSourceDownloader->getSourceCode($urlToScrape);
        $websiteDom = $simpleHtmlDomFactory->makeFromWebsiteSource($source);
        $this->issuedDate = $issuedDateScraper->scrape($websiteDom);
        $this->csvZipFileName = rtrim($baseUrl, '/') . '/' . ltrim($csvZipFileNameScraper->scrape($websiteDom), '/');
    }

    public function getIssuedDate(): string
    {
        return $this->issuedDate;
    }

    public function getCsvZipFileName(): string
    {
        return $this->csvZipFileName;
    }
}

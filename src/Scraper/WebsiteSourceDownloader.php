<?php

namespace App\Scraper;

class WebsiteSourceDownloader
{

    public function getSourceCode(string $url): string
    {
        $source = \file_get_contents($url);
        if ($source === false) throw new \Exception('Failed to downoad website source from URL: ' . $url);
        else return $source;
    }
}

<?php

namespace App\Scraper;

use simple_html_dom;

require_once(__DIR__ . '/simple_html_dom.php');

class SimpleHtmlDomFactory{

    public function makeFromWebsiteSource(string $websiteSource): simple_html_dom
    {
        return \str_get_html($websiteSource);
    }
}
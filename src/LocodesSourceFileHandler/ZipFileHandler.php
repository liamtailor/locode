<?php

namespace App\LocodesSourceFileHandler;

use App\Exceptions\FileUnlinkUnsuccessfullException;

class ZipFileHandler
{
    private const TEMP_ZIP_FILE_NAME = 'locodes_temp.zip';

    /** @var \ZipArchive $zipHandler */
    private $zipHandler;

    /** @var string $extractToPath */
    private $extractToPath;

    public function __construct(\ZipArchive $zipHandler, string $extractToPath)
    {
        $this->zipHandler = $zipHandler;
        $this->extractToPath = rtrim($extractToPath, '/') . '/';
    }

    /** 
     * @throws \Exception
     * @throws FileUnlinkUnsuccessfullException
     */
    public function unzip(string $filePath, bool $deleteZipAfterUnpacking = true)
    {
        // TODO: Clear folder before downloading new file and/or after importing csv contents to database?
        // TODO: Validate given filePath if file exists and extension is zip
        $localZipFilePath = $this->extractToPath . self::TEMP_ZIP_FILE_NAME;
        if (($downloadedZip = \file_get_contents($filePath)) === false) throw new \Exception('File download unsuccessfull: ' . $filePath);
        if (\file_put_contents($localZipFilePath, $downloadedZip) === false) throw new \Exception('Could not write downloaded file to local storage: ' . $localZipFilePath);
        else echo 'New ZIP file downloaded to ' . $localZipFilePath . PHP_EOL;

        if ($this->zipHandler->open($localZipFilePath) === true) {
            $this->zipHandler->extractTo($this->extractToPath);
            $this->zipHandler->close();
            if ($deleteZipAfterUnpacking) {
                $wasUnlinkSuccessfull = \unlink($localZipFilePath);
                if ($wasUnlinkSuccessfull === false) throw new FileUnlinkUnsuccessfullException('Could not delete ZIP file after unzipping: ' . $localZipFilePath);
            }
        } else {
            throw new \Exception('Could not unzip file: ' . $localZipFilePath);
        }
    }
}

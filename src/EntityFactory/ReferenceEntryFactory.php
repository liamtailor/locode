<?php

namespace App\EntityFactory;

use App\Entity\ReferenceEntry;
use App\Entity\UnLocode;
use App\Repository\UnLocodeRepository;
use Exception;

class ReferenceEntryFactory
{
    /** @var UnLocodeRepository $unLocodeRepository */
    private $unLocodeRepository;

    public function __construct(UnLocodeRepository $unLocodeRepository)
    {
        $this->unLocodeRepository = $unLocodeRepository;
    }

    /** @return UnLocode[] */
    public function makeEntitiesFromRecords(\Generator $records): array
    {
        foreach ($records as $record) {
            if (!$this->recordIsReferenceEntry($record)) continue;

            $referenceEntry = new ReferenceEntry();
            try {
                $unLocode = $this->unLocodeRepository->findByCountryCodeAndNameWithoutDiacriticsOrCode($record[1], $record[4]);
                if (is_null($unLocode)) throw new \Exception('No record found with given name. Might be misspelled.');
            } catch (\Exception $e) {
                echo 'Error: ' . $e->getMessage() . PHP_EOL . 'Given record mith be ambiguous:  ' . PHP_EOL . print_r($record, true);
                continue;
            }
            $referenceEntry
                ->setUnLocode($unLocode)
                ->setChangeIndicator($record[0])
                ->setLocodeCountry($record[1])
                ->setName($record[3])
                ->setNameWithoutDiacritics($record[4]);

            $allReferenceEntries[] = $referenceEntry;
        }

        return $allReferenceEntries ?? [];
    }

    private function recordIsReferenceEntry(array $record)
    {
        return $record[0] === '=' || (empty($record[0]) && empty($record[2]) && !empty($record[4]));
    }
}

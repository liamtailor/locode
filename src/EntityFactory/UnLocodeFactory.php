<?php

namespace App\EntityFactory;

use App\Entity\UnLocode;

class UnLocodeFactory
{
    /** @var string[] $usedLocodes */
    private $usedLocodes  = [];

    /** @return UnLocode[] */
    public function makeEntitiesFromRecords(\Generator $records): array
    {
        foreach ($records as $record) {
            if ($this->recordIsInvalid($record)) continue;

            $unLocode = new UnLocode();
            $unLocode
                ->setChangeIndicator($record[0])
                ->setLocodeCountry($record[1])
                ->setLocodeLocation($record[2])
                ->setName($record[3])
                ->setNameWithoutDiacritics($record[4])
                ->setSubdivision($record[5])
                ->setFunction($record[6])
                ->setStatus($record[7])
                ->setDate($record[8])
                ->setIata($record[9])
                ->setCoordinates($record[10])
                ->setRemarks($record[11]);

            $allUnLocodes[] = $unLocode;
        }

        return $allUnLocodes ?? [];
    }

    private function recordIsInvalid(array $record)
    {
        return $record[0] === '=' || empty($record[2]);
    }
}

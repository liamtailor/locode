<?php

namespace App\LocodeDatabaseUpdater;

use League\Csv\Reader;
use League\Csv\ResultSet;
use League\Csv\Statement;

class LocodeCsvFileParser
{
    /** @throws \Exception */
    public function getRecordsFromFile(string $filePath = __DIR__ . '/../../tests/fixtures/UNLOCODE List.csv'): ResultSet
    {
        if (\file_exists($filePath)) {
            $csv = Reader::createFromPath($filePath, 'r');
            return (new Statement())->process($csv);
        } else {
            throw new \Exception('Given file does not exist:' . $filePath);
        }
    }
}

<?php

namespace App\LocodeDatabaseInformation;

class LocodeDatabaseCurrentIssueDate extends LocodeDatabaseVariable
{
    protected function getPathToFileStoringVariable(): string
    {
        return __DIR__ . '/current_issue_date_var';
    }
}

<?php

namespace App\LocodeDatabaseInformation;

class VariableToFileWriter
{
    /** @throws \Exception */
    public function writeValueToFile(string $filePath, string $value): void
    {
        if (\file_put_contents($filePath, $value) === false)
            throw new \Exception('Could not save new variable (value:"' . $value . '") value to file: ' . $filePath);
    }
}

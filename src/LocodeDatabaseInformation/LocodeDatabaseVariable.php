<?php

namespace App\LocodeDatabaseInformation;

abstract class LocodeDatabaseVariable
{
    /** @var string $currentValue */
    private $currentValue;
    /** @var VariableToFileWriter $variableToFileWriter */
    protected $variableToFileWriter;

    public function __construct(VariableToFileWriter $variableToFileWriter)
    {
        // TODO: Should use some better way to store these values.
        $filePath = $this->getPathToFileStoringVariable();
        if (\file_exists($filePath) === false) \touch($filePath);
        $this->currentValue = \file_get_contents($filePath);
        $this->variableToFileWriter = $variableToFileWriter;
    }

    public function getCurrentValue(): string
    {
        return $this->currentValue;
    }

    /** @throws \Exception */
    public function setNewValue(string $value): void
    {
        $this->currentValue = $value;
        $this->variableToFileWriter->writeValueToFile($this->getPathToFileStoringVariable(), $value);
    }

    protected abstract function getPathToFileStoringVariable(): string;
}

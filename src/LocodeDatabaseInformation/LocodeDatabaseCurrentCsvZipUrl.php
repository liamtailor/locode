<?php

namespace App\LocodeDatabaseInformation;

class LocodeDatabaseCurrentCsvZipUrl extends LocodeDatabaseVariable
{
    protected function getPathToFileStoringVariable(): string
    {
        return __DIR__ . '/csv_zip_file_path_var';
    }
}

<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use App\Entity\ReferenceEntry;
use App\Formatter\LocodeLocationNameFormatter;
use JsonSerializable;

/**
 * @ORM\Table(name="locodes",
 *    uniqueConstraints={
 *        @UniqueConstraint(name="locode_unique", 
 *            columns={"locode_country", "locode_location", "name"})
 *    },
 *    options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\UnLocodeRepository")
 */
class UnLocode implements JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="ReferenceEntry", mappedBy="unLocode")
     */
    private $referringEntries;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $changeIndicator;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $locodeCountry;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $locodeLocation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nameWithoutDiacritics;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $subdivision;

    /**
     * @ORM\Column(type="string", length=8)
     */
    private $function;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $iata;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $coordinates;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $remarks;

    public function __construct()
    {
        $this->referringEntries = new ArrayCollection();
    }

    public function jsonSerialize()
    {
        foreach ($this->getReferringEntries() as $referringEntry) {
            $referringEntries[] =
                [
                    'change_indicator' => $referringEntry->getChangeIndicator(),
                    'locode' => $referringEntry->getLocodeCountry(),
                    'name' => $referringEntry->getName(),
                    'name_without_diacritics' => $referringEntry->getNameWithoutDiacritics()
                ];
        }
        return
            [
                'change_indicator' => $this->getChangeIndicator(),
                'locode' => $this->getLocodeCountry() . '/' . $this->getLocodeLocation(),
                'name' => $this->getName(),
                'name_without_diacritics' => $this->getNameWithoutDiacritics(),
                'subdivision' => $this->getSubdivision(),
                'function' => $this->getFunction(),
                'status' => $this->getStatus(),
                'date' => $this->getDate(),
                'iata' => $this->getIata(),
                'coordinates' => $this->getCoordinates(),
                'remarks' => $this->getRemarks(),
                'reffering entries' => $referringEntries ?? []
            ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChangeIndicator(): ?string
    {
        return $this->changeIndicator;
    }

    public function setChangeIndicator(string $changeIndicator): self
    {
        $changeIndicator = utf8_encode($changeIndicator);
        $this->setEmptyFieldToNull($changeIndicator);
        $this->changeIndicator = $changeIndicator;

        return $this;
    }

    public function getLocodeCountry(): ?string
    {
        return $this->locodeCountry;
    }

    public function setLocodeCountry(string $locodeCountry): self
    {
        $this->locodeCountry = $locodeCountry;

        return $this;
    }

    public function getLocodeLocation(): ?string
    {
        return $this->locodeLocation;
    }

    public function setLocodeLocation(string $locodeLocation): self
    {
        $this->locodeLocation = $locodeLocation;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getSubdivision(): ?string
    {
        return $this->subdivision;
    }

    public function setSubdivision(string $subdivision): self
    {
        $this->subdivision = $subdivision;

        return $this;
    }

    public function getFunction(): ?string
    {
        return $this->function;
    }

    public function setFunction(string $function): self
    {
        $this->function = $function;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(string $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getIata(): ?string
    {
        return $this->iata;
    }

    public function setIata(string $iata): self
    {
        $this->setEmptyFieldToNull($iata);
        $this->iata = $iata;

        return $this;
    }

    public function getCoordinates(): ?string
    {
        return $this->coordinates;
    }

    public function setCoordinates(string $coordinates): self
    {
        $this->setEmptyFieldToNull($coordinates);
        $this->coordinates = $coordinates;

        return $this;
    }

    public function getRemarks(): ?string
    {
        return $this->remarks;
    }

    public function setRemarks(string $remarks): self
    {
        $remarks = utf8_encode($remarks);
        $this->setEmptyFieldToNull($remarks);
        $this->remarks = $remarks;

        return $this;
    }

    public function setName(string $name): self
    {
        $this->name = utf8_encode($name);

        return $this;
    }

    public function getNameWithoutDiacritics(): ?string
    {
        return $this->nameWithoutDiacritics;
    }

    public function setNameWithoutDiacritics(string $nameWithoutDiacritics): self
    {
        $nameWithoutDiacritics = utf8_encode($nameWithoutDiacritics);
        LocodeLocationNameFormatter::removeAlternativeName($nameWithoutDiacritics);
        $this->nameWithoutDiacritics = $nameWithoutDiacritics;

        return $this;
    }

    /**
     * @return Collection|LocationName[]
     */
    public function getReferringEntries(): Collection
    {
        return $this->referringEntries;
    }

    public function addReferringEntry(LocationName $referringEntry): self
    {
        if (!$this->referringEntries->contains($referringEntry)) {
            $this->referringEntries[] = $referringEntry;
            $referringEntry->setUnLocode($this);
        }

        return $this;
    }

    public function removeReferringEntry(LocationName $referringEntry): self
    {
        if ($this->referringEntries->contains($referringEntry)) {
            $this->referringEntries->removeElement($referringEntry);
            // set the owning side to null (unless already changed)
            if ($referringEntry->getUnLocode() === $this) {
                $referringEntry->setUnLocode(null);
            }
        }

        return $this;
    }

    /** Set to null instead of empty string to get a true NULL value in database */
    private function setEmptyFieldToNull(&$field): void
    {
        if (empty($field))
            $field = null;
    }
}

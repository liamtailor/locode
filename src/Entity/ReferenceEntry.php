<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Table(name="reference_entries",
 *    uniqueConstraints={
 *        @UniqueConstraint(name="reference_entry_unique", 
 *            columns={"locode_country", "name"})
 *    },
 *    options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\ReferenceEntryRepository")
 */
class ReferenceEntry
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UnLocode", inversedBy="referringEntries")
     * @ORM\JoinColumn(name="refers_to", referencedColumnName="id")
     */
    private $unLocode;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $changeIndicator;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $locodeCountry;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nameWithoutDiacritics;

    /** Set to null instead of empty string to get a true NULL value in database */
    private function setEmptyFieldToNull(&$field): void
    {
        if (empty($field))
            $field = null;
    }

    public function getChangeIndicator(): ?string
    {
        return $this->changeIndicator;
    }

    public function setChangeIndicator(?string $changeIndicator): self
    {
        $this->changeIndicator = $changeIndicator;

        return $this;
    }

    public function getLocodeCountry(): ?string
    {
        return $this->locodeCountry;
    }

    public function setLocodeCountry(string $locodeCountry): self
    {
        $this->locodeCountry = $locodeCountry;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = utf8_encode($name);

        return $this;
    }

    public function getNameWithoutDiacritics(): ?string
    {
        return $this->nameWithoutDiacritics;
    }

    public function setNameWithoutDiacritics(string $nameWithoutDiacritics): self
    {
        $this->nameWithoutDiacritics = utf8_encode($nameWithoutDiacritics);

        return $this;
    }

    public function getUnLocode(): ?UnLocode
    {
        return $this->unLocode;
    }

    public function setUnLocode(?UnLocode $unLocode): self
    {
        $this->unLocode = $unLocode;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }
}

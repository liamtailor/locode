#!/bin/bash
# Or do them one after another by hand if your PATH doesn't contain either composer, php or symfony.
symfony check:requirements &&
composer install --no-dev --optimize-autoloader &&
php bin/console doctrine:database:create &&
php bin/console doctrine:schema:update --force &&
php bin/console app:import-locode-entities-to-database &&
symfony server:start


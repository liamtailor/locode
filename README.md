### UN LOCODE API ###

Obecnie w formie szybkiego prototypu z małą ilością testów i brakami w obsłudze wyjątków.
Po napisaniu testów całość wymaga refaktoryzacji. Zwłaszcza klasa `src/Command/ImportUnLocodeEntitiesToDatabaseCommand` powinna być rozbita na wiele mniejszych klas.

API za pomocą komendy `php bin/console app:import-locode-entities-to-database` sprawdza, czy pojawiła się nowa wersja bazy kodów oraz pobiera plik ZIP zapisując kody do bazy danych. Wykonanie komendy może trwać kilka minut, ze względu na ilość rekordów (ponad 100k).

#### Komplikacje w plikach CSV ####

Kilka rekordów ma literówki, przez co nie są rozpoznane jako referencje (np. "Nizhny Novgorod" zamiast "Nizhniy Novgorod").
Jedna z referencji odnosi się do wielu miejsc, więc również jest ignorowana ("Espoonlahti = Espoo" -> "FI/ESP Espoo (Esbo)" oraz "FI/ESP Esbo (Espoo)" i "FI/ES2 Espoo"), zakladam, że referencje powinny byc jednoznaczne.

Ze względu na różne formy zapisu referencji i alternatywnych nazw (po znakach: "=" i "/", lub w nawiasach) kolumna w tabeli 'locodes' przechowująca nazwę bez znaków diakrytycznych usuwa te alternatywne zapisy, aby możliwe było odnalezienie wpisów-referencji, które zawierają tylko podstawową nazwę (np. referencja "Three Rivers = Trois-Rivières" która odnosi się do wpisu "Trois-Rivieres (Three Rivers)").
Jeśli chcielibyśmy zachować te dodatkowe nazwy można utworzyć dodatkową kolumnę, która będzie przechowywała nazwę w taki sposób i to ona będzie używana do odszukiwania referencji.

#### Korzystanie z API ####

Do wyświetlania danych służą:
/locode/get_by_code?country_code=AE&location_code=ALB -> edge który wyświetla dane po podaniu LOCODE.
/locode/get_by_name?name_without_diacritics=Antwerpen -> edge który wyświetla dane po podaniu nazwy miejsca bez znaków diakrytycznych.

Obecnie rekordy w bazie nie są aktualizowane (ON DUPLICATE KEY UPDATE), więc aby zapisać dane z nowych plików należy wyczyścić obie tabele (TRUNCATE bez sprawdzania kluczy obcych).

<?php

namespace App\Tests\Unit\Entity;

use App\Entity\UnLocode;
use PHPUnit\Framework\TestCase;

class UnLocodeTest extends TestCase
{
    public function testSetNameWithoutDiacritics_RemovesAlternativeNameInCurlyBrackest()
    {
        $nameWithoutDiacritics = 'Trois-Rivieres (Three Rivers)';
        $expected = 'Trois-Rivieres';
        $entity = new UnLocode();
        $entity->setNameWithoutDiacritics($nameWithoutDiacritics);

        $actual = $entity->getNameWithoutDiacritics();

        $this->assertEquals($expected, $actual);
    }

    public function testSetNameWithoutDiacritics_ChangesFormatToUtf8()
    {
        $nameWithoutDiacritics = 'Pointe d’Esny';
        $expected = \utf8_encode($nameWithoutDiacritics);
        $entity = new UnLocode();
        $entity->setNameWithoutDiacritics($nameWithoutDiacritics);

        $actual = $entity->getNameWithoutDiacritics();

        $this->assertEquals($expected, $actual);
    }

    
    public function testSetName_ChangesFormatToUtf8()
    {
        $name = 'Pointe d’Esny';
        $expected = \utf8_encode($name);
        $entity = new UnLocode();
        $entity->setName($name);

        $actual = $entity->getName();

        $this->assertEquals($expected, $actual);
    }
}

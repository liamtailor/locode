<?php

namespace App\Tests\Unit\LocodeDatabaseUpdater;

use App\LocodeDatabaseUpdater\LocodeCsvFileParser;
use PHPUnit\Framework\TestCase;

class LocodeCsvFileParserTest extends TestCase
{
    public function testGetRecordsFromFile()
    {
        $parser = new LocodeCsvFileParser();

        $resultSet = $parser->getRecordsFromFile(__DIR__ . '/../../fixtures/UNLOCODE List.csv');

        foreach($resultSet as $record){
            $this->assertIsArray($record);
            $this->assertCount(12, $record);
        }
    }
}

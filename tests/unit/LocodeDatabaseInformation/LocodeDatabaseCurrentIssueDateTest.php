<?php

namespace App\Tests\unit\LocodeDatabaseInformation;

use App\LocodeDatabaseInformation\LocodeDatabaseCurrentIssueDate;
use App\LocodeDatabaseInformation\LocodeDatabaseVariable;
use PHPUnit\Framework\MockObject\MockObject;

class LocodeDatabaseCurrentIssueDateTest extends LocodeDatabaseVariableTestAbstract
{
    public function testGetAndSet()
    {
        $this->testSetsAndRetrievesGivenValue();
    }

    protected function getTestedObject(MockObject $variableToFileWriter): LocodeDatabaseVariable
    {
        return new LocodeDatabaseCurrentIssueDate($variableToFileWriter);
    }
}

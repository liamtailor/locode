<?php

namespace App\Tests\unit\LocodeDatabaseInformation;

use App\LocodeDatabaseInformation\LocodeDatabaseCurrentCsvZipUrl;
use App\LocodeDatabaseInformation\LocodeDatabaseVariable;
use PHPUnit\Framework\MockObject\MockObject;

class LocodeDatabaseCurrentCsvZipUrlTest extends LocodeDatabaseVariableTestAbstract
{
    public function testGetAndSet()
    {
        $this->testSetsAndRetrievesGivenValue();
    }

    protected function getTestedObject(MockObject $variableToFileWriter): LocodeDatabaseVariable
    {
        return new LocodeDatabaseCurrentCsvZipUrl($variableToFileWriter);
    }
}

<?php

namespace App\Tests\unit\LocodeDatabaseInformation;

use App\LocodeDatabaseInformation\LocodeDatabaseVariable;
use App\LocodeDatabaseInformation\VariableToFileWriter;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

abstract class LocodeDatabaseVariableTestAbstract extends TestCase
{
    /** @throws \Exception */
    protected function testSetsAndRetrievesGivenValue(): void
    {
        $expected = 'some value';
        $variableToFileWriter = $this->createMock(VariableToFileWriter::class);
        $variableToFileWriter
            ->expects($this->once())
            ->method('writeValueToFile')
            ->with($this->callback(
                function (string $filePath, string $value) use ($expected) {
                    // TODO: For some reason second argument of writeValueToFile method doesn't get passed to this callback
                    $this->assertIsString($filePath);
                    $this->assertTrue(\file_exists($filePath));
                    $this->assertEquals($expected, $value);
                    return true;
                }
            ));
        $tested = $this->getTestedObject($variableToFileWriter);

        $tested->setNewValue($expected);
        $actual = $tested->getCurrentValue();

        $this->assertEquals($expected, $actual);
    }

    protected abstract function getTestedObject(MockObject $variableToFileWriter): LocodeDatabaseVariable;
}

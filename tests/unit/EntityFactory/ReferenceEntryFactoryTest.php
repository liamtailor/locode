<?php

namespace App\Tests\Unit\EntityFactory;

use App\Entity\UnLocode;
use App\EntityFactory\ReferenceEntryFactory;
use App\Repository\UnLocodeRepository;
use Generator;
use League\Csv\ResultSet;
use PHPUnit\Framework\TestCase;

class ReferenceEntryFactoryTest extends TestCase
{
    /** @dataProvider provider_testMakeEntitiesFromResultSet */
    public function testMakeEntitiesFromResultSet(array $record)
    {
        $unLocode = $this->createMock(UnLocode::class);
        $iterator = new \ArrayIterator([$record]);
        $generator = $this->getGenerator($iterator);
        $repository = $this->createMock(UnLocodeRepository::class);
        $repository->expects($this->once())->method('findByCountryCodeAndNameWithoutDiacriticsOrCode')->with($record[1], $record[4])->willReturn($unLocode);
        $factory = new ReferenceEntryFactory($repository);

        $actual = $factory->makeEntitiesFromRecords($generator);

        $this->assertIsArray($actual);
        $this->assertCount(1, $actual);
        $referenceEntry = $actual[0];
        $this->assertSame($unLocode, $referenceEntry->getUnLocode());
        $this->assertEquals($record[0], $referenceEntry->getChangeIndicator());
        $this->assertEquals($record[1], $referenceEntry->getLocodeCountry());
        $this->assertEquals($record[3], $referenceEntry->getName());
        $this->assertEquals($record[4], $referenceEntry->getNameWithoutDiacritics());
    }

    private function getGenerator(\ArrayIterator $iterator): Generator
    {
        foreach ($iterator as $index => $value) {
            yield $index => $value;
        }
    }

    public function provider_testMakeEntitiesFromResultSet()
    {
        return
            [
                [
                    ['=', 'AE', '', 'Ruwais = Ar Ruways', 'Ruwais = Ar Ruways', '', '', '', '', '', '', '']
                ],
                [
                    ['', 'FR', '', 'Basse-Terre = GP BBR', 'Basse-Terre = GP BBR', '', '1---5---', 'AF', '9506', '', '', '']
                ],
                [
                    ['', 'FR', '', 'Saint-Denis = RERUN', 'Saint-Denis = RERUN', '', '1---5---', 'AF', '9506', '', '', '']
                ],
            ];
    }
}

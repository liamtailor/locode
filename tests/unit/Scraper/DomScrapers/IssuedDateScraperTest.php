<?php

namespace App\Tests\Unit\Scraper\DomScrapers;

use App\Scraper\DomScrapers\IssuedDateScraper;

class IssuedDateScraperTest extends AbstractDomScraperTest
{
    public function testScrape()
    {
        $expected = '08-07-2019';
        $scraper = new IssuedDateScraper();

        $actual = $scraper->scrape($this->dom);

        $this->assertEquals($expected, $actual);
    }
}

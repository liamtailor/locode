<?php

namespace App\Tests\Unit\Scraper\DomScrapers;

require_once(__DIR__ . '/../../../../src/Scraper/simple_html_dom.php'); // Needed for class definition during unserializing.

use PHPUnit\Framework\TestCase;
use \simple_html_dom;

class AbstractDomScraperTest extends TestCase
{
    public function setUp(): void
    {
        $serialized = \file_get_contents(__DIR__ . '/../../../fixtures/unece_org_website_simple_html_dom_serialized');
        $this->dom = \unserialize($serialized);
    }
}

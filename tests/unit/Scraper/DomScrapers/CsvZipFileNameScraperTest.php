<?php

namespace App\Tests\Unit\Scraper\DomScrapers;

use App\Scraper\DomScrapers\CsvZipFileNameScraper;
class CsvZipFileNameScraperTest extends AbstractDomScraperTest
{
    public function testScrape()
    {
        $expected = '/fileadmin/DAM/cefact/locode/loc191csv.zip';
        $scraper = new CsvZipFileNameScraper();

        $actual = $scraper->scrape($this->dom);

        $this->assertEquals($expected, $actual);
    }
}

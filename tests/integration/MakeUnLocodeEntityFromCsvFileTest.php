<?php

namespace App\Tests\Integration;

use App\Entity\UnLocode;
use App\EntityFactory\UnLocodeFactory;
use App\LocodeDatabaseUpdater\LocodeCsvFileParser;
use PHPUnit\Framework\TestCase;

class MakeUnLocodeEntityFromCsvFileTest extends TestCase
{
    public function testMakeEntitiesFromResultSet_ReturnsValidEntitiesFromFixtureCsvFile()
    {
        $parser = new LocodeCsvFileParser();
        $factory = new UnLocodeFactory();

        $resultSet = $parser->getRecordsFromFile(__DIR__ . '/../fixtures/UNLOCODE List.csv');
        $entities = $factory->makeEntitiesFromResultSet($resultSet->getRecords());

        $this->assertCount(3, $entities);
        foreach($entities as $entity){
            $this->assertInstanceOf(UnLocode::class, $entity);
        }
    }
}

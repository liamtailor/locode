<?php

namespace App\Tests\Functional\Repository;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductRepositoryFunctionalTest extends WebTestCase
{
    public function setUp(): void
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->em = static::$kernel->getContainer()
            ->get('doctrine.orm.entity_manager');
    }

    public function testFindOneByLocode_RetrunsNullWithEmptyArguments()
    {
        $unLocode = $this->em->getRepository('App:UnLocode')->findOneByLocode('', '');

        self::assertNull($unLocode);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->em->close();
    }
}
